# What I've tried so FAR

-Add everything as diratives in javascripts/application.js , stylesheets/application.css with require_tree . , then put in /config/initializers/assets.rb
the application.js , application.css for precompilation (tried also without adding them)
Result: everything was broken.

-Tried to call javascript_include_tag both in <head> and <body> of the template

-Tried setting config.assets.enabled = false in config/application.rb

-Tried setting precompilation array from /config/initializers/assets.rb to [ ]

-Also played abit with config.assets in environements/production.rb and development.rb but no luck

# The error always is 
"Asset was not declared to be precompiled in production.
Add `Rails.application.config.assets.precompile += %w( bootstrap/css/bootstrap.css )` to `config/initializers/assets.rb` and restart your server"